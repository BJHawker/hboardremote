unit wndSelectPlan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DB, DBClient, WinXP, MPlayer, ExtCtrls;

type
  TfrmSelectPlan = class(TForm)
    bbtnClose: TBitBtn;
    lbItems: TListBox;
    bbtnBack: TBitBtn;
    pnlDisplay: TPanel;
    imgMain: TImage;
    mpMain: TMediaPlayer;
    bbtnDelete: TBitBtn;
    bbtnRestore: TBitBtn;
    bbtnAdd: TBitBtn;
    odAddFile: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure bbtnCloseClick(Sender: TObject);
    procedure lbItemsClick(Sender: TObject);
    procedure bbtnBackClick(Sender: TObject);
    procedure mpMainNotify(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure bbtnDeleteClick(Sender: TObject);
    procedure bbtnRestoreClick(Sender: TObject);
    procedure lbItemsDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure bbtnAddClick(Sender: TObject);
  private

    FLevel: Integer;
    FCurrentFolder: String;

    lstStatus : TStringList;
    MediaPlayerCommandLine : String;
    lModifications,
    lPlaying : Boolean;
    fProcessToTerminate : THandle;
    procedure SetLevel(const Value: Integer);
    procedure ReadMonitors;
    procedure ReadFiles(_Mask: String);
    procedure DisplayMedia(_MediaToDisplay: String);
    procedure DisplayImage(_FileName : String = '');
    procedure PlayVideo(_FileName: String);
    procedure StopVideo;
    procedure EndVideo(Sender: TObject);
    procedure SetButtonsStatus;
    function CheckModifications() : Boolean;
    procedure ApplyModifications;
    function IsFileDeleted(_FileName: String): Boolean;
    function IsFileAdded(_FileName : String) : Boolean;
    procedure AddFile(_FileName : String);
  public
    property Level : Integer read FLevel write SetLevel;
    property CurrentFolder : String read FCurrentFolder;
  end;

  function GetProcessId(Process: THandle): DWORD; stdcall; external 'kernel32.dll' name 'GetProcessId';

var
  frmSelectPlan: TfrmSelectPlan;

implementation

uses uCommon, uHBoardFunctions, Math, uBoardLiteCommon, uCSV2DBfunctions,
  StrUtils, iniFiles;
{$R *.dfm}

Const
  CST_INI_EXTENSION = '.ini';
  CST_CONFIG_SECTION = 'Config';
  CST_VIDEO_COMMANDLINE = 'VideoPlayer_CommandLine';
  CST_LAST_IMAGES_FOLDER = 'LastImagesFolder';
  CST_DELETED = 'Deleted';
  CST_ADDED = 'Added';
  CST_DELETED_EXTENSION = '.DELETED';
  CST_ADDED_EXTENSION = '.ADDED';

{Type
  TEnumData = Record
    hW: HWND;
    pID: DWORD;
  End;

Function EnumProc( hw: HWND; Var data: TEnumData ): Bool; stdcall;
  Var
    pID: DWORD;
  Begin
    Result := True;
    If (GetWindowLong(hw, GWL_HWNDPARENT) = 0) and
       (IsWindowVisible( hw ) or IsIconic(hw)) and
       ((GetWindowLong(hw, GWL_EXSTYLE) and WS_EX_APPWINDOW) <> 0)
    Then Begin
      GetWindowThreadProcessID( hw, @pID );
      If pID = data.pID Then Begin
        data.hW := hW;
        Result := False;
      End;
    End;
  End;

Function WindowFromProcessID( pID: DWORD ): HWND;
  Var
    data: TEnumData;
  Begin
    data.pID := pID;
    data.hW := 0;
    EnumWindows( @EnumProc, longint(@data) );
    Result := data.hW;
  End;
  }

procedure TfrmSelectPlan.FormCreate(Sender: TObject);
var
  iniFile : TiniFile;
begin

  lstStatus := TStringList.Create();
  Level := 0;
  iniFile := TIniFile.Create(AddSlash(ExtractFilePath(Application.ExeName)) + ExtractOnlyFileName(Application.ExeName) + CST_INI_EXTENSION);
  MediaPlayerCommandLine := iniFile.ReadString(CST_CONFIG_SECTION, CST_VIDEO_COMMANDLINE, '"c:\Program Files (x86)\K-Lite Codec Pack\MPC-HC64\mpc-hc64.exe" "%MediaFileName%" /play /fixedsize %width%,%height% /close /slave %hWnd% /monitor %MonitorNo%');
  odAddFile.InitialDir := iniFile.ReadString(CST_CONFIG_SECTION, CST_LAST_IMAGES_FOLDER, ExtractFilePath(Application.ExeName));
  iniFile.Free();
  ShowWindow(FindWindow('Shell_TrayWnd', nil), SW_HIDE);

end;

procedure TfrmSelectPlan.bbtnCloseClick(Sender: TObject);
begin
  if CheckModifications() then Close();
end;

procedure TfrmSelectPlan.ReadMonitors();
var
  nCount : Integer;
  MonitorCSV:TStringList;
  sFileName : String;
begin
  sFileName := ExtractFilePath(Application.Exename)+'monitor.csv';
  if FileExists(sFileName) then
  begin
    lbItems.Items.Clear();
    MonitorCSV := TStringList.Create();
    MonitorCSV.LoadFromFile(sFileName);
    for nCount := 1 to MonitorCSV.count-1 do
      lbItems.Items.Add(GetCSVcell(MonitorCSV[nCount],1));
    MonitorCSV.Free();
  end
  else
  begin
    ShowMessage('File ' + sFileName + ' not found. Application will terminate !');
    Close();
  end;
end;

procedure TfrmSelectPlan.ReadFiles(_Mask : String);
var
  nCount : Integer;
begin
  lstStatus.Clear();
  GetAllFilesFromMask(_Mask, lbItems.Items);
  for nCount := 0 to lbItems.Count-1 do
    if IsFileDeleted(lbItems.Items[nCount]) then
      lstStatus.Add(CST_DELETED)
    else if IsFileAdded(lbItems.Items[nCount]) then
      lstStatus.Add(CST_ADDED)
    else
      lstStatus.Add('');
  lModifications := False;
  lbItems.Refresh();
end;

procedure TfrmSelectPlan.SetLevel(const Value: Integer);
begin
  FLevel := Min(1, Value);
  if FLevel = 1 then
  begin
    FCurrentFolder := ExtractFilePath(ActualValue(lbItems));
    ReadFiles(ActualValue(lbItems));
  end
  else
  begin
    ReadMonitors();
    FCurrentFolder := '';
  end;
  SetButtonsStatus();
end;

procedure TfrmSelectPlan.SetButtonsStatus();
begin
  bbtnBack.Enabled := (Level = 1);
  bbtnDelete.Visible := (Level = 1) and (lbItems.ItemIndex > -1) and not (lstStatus[lbItems.ItemIndex] = CST_DELETED);
  bbtnRestore.Visible := (Level = 1) and (lbItems.ItemIndex > -1) and (lstStatus[lbItems.ItemIndex] = CST_DELETED);
  bbtnAdd.Visible := (Level = 1);
end;

procedure TfrmSelectPlan.lbItemsClick(Sender: TObject);
begin
  SetButtonsStatus();
  if lPlaying then StopVideo();
  If (Level = 0) and not (ActualValue(lbItems) = '') then
    Level := 1
  else
    DisplayMedia(ActualValue(lbItems));
end;

procedure TfrmSelectPlan.bbtnBackClick(Sender: TObject);
begin
  if CheckModifications() then
  begin
    StopVideo();
    DisplayImage();
    If Level = 1 then Level := 0;
  end;
end;

procedure TfrmSelectPlan.DisplayMedia(_MediaToDisplay : String);
begin
  if FileExists(_MediaToDisplay) then
    Case MediaType(_MediaToDisplay) of
      mtVideo :   PlayVideo(_MediaToDisplay);
      mtPicture : DisplayImage(_MediaToDisplay);
      mtUndefined : DisplayImage();
    end;
end;

procedure TfrmSelectPlan.PlayVideo(_FileName : String);
var
  CommandLine : String;
  ProcessInformation : TProcessInformation;
//  ProcessId : DWORD;
//  hWndMediaPlayer : HWND;
begin
  DisplayImage();
  CommandLine := ReplaceString(MediaPlayerCommandLine, '%MediaFileName%', _FileName);
  CommandLine := ReplaceString(CommandLine, '%MonitorNo%', IntToStr(Self.Monitor.MonitorNum));
//  CommandLine := ReplaceString(CommandLine, '%Width%', IntToStr(pnlDisplay.Width));
//  CommandLine := ReplaceString(CommandLine, '%Height%', IntToStr(pnlDisplay.Height));
  CommandLine := ReplaceString(CommandLine, '%hWnd%', IntToStr(Application.Handle));
  if StartProcess(CommandLine, Self.Monitor, ProcessInformation, Top + pnlDisplay.Top, Left + pnlDisplay.Left, pnlDisplay.Width, pnlDisplay.Height) then
  begin
//    ProcessId := GetProcessId(ProcessInformation.hProcess);
//    hWndMediaPlayer := WindowFromProcessID(ProcessId);
//    ShowMessage(IntToStr(hWndMediaPlayer));
//    SetWindowPos(hWndMediaPlayer, 0, pnlDisplay.Left, pnlDisplay.Top, pnlDisplay.Width, pnlDisplay.Height,SWP_ASYNCWINDOWPOS);
    lPlaying := True;
    TEndProcessThread.Create(ProcessInformation.hProcess, EndVideo);
  end
  else
    ShowMessage('Impossible de lancer la vid�o via la commande ' + CommandLine);
  fProcessToTerminate := ProcessInformation.hProcess;
end;

procedure TfrmSelectPlan.StopVideo();
begin
  if not(fProcessToTerminate = 0) then
  begin
    KillProcess(fProcessToTerminate);
    fProcessToTerminate := 0;
  end;
end;

procedure TfrmSelectPlan.DisplayImage(_FileName : String = '');
var
  lLoaded : Boolean;
  Pict:TPicture;
begin
  lLoaded := false;
  imgMain.Visible := not (_FileName = '');
  if FileExists(_FileName) then
  begin
    Pict:=TPicture.Create;
    Try
      Pict.LoadFromFile(_FileName);
      lLoaded := True;
    Except
      ShowMessage('Impossible de charger ' + _FileName);
    End;

    if lLoaded then
    begin
      imgMain.Picture:=Pict;
      Stretch(imgMain, Rect(1, 1, pnlDisplay.Width-1, pnlDisplay.Height-1));
    end;

    Pict.Free();
  end;
end;

procedure TfrmSelectPlan.mpMainNotify(Sender: TObject);
begin
  with Sender as TMediaPlayer do 
  begin
    case Mode of
      mpStopped: lPlaying := False;
    end;
    Notify := True;
  end;
end;

procedure TfrmSelectPlan.EndVideo(Sender: TObject);
begin
end;

procedure TfrmSelectPlan.FormActivate(Sender: TObject);
begin
  if lPlaying then
  begin
    Beep;
    StopVideo();
  end;
end;

procedure TfrmSelectPlan.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  StopVideo();
  ShowWindow(FindWindow('Shell_TrayWnd', nil), SW_NORMAL);
  lstStatus.Free();
end;

procedure TfrmSelectPlan.FormResize(Sender: TObject);
begin
  lbItems.Width := Width - 16;
  lbItems.Height := 252;
  pnlDisplay.Width := Width - 16;
  pnlDisplay.Top := lbItems.Top + lbItems.Height + 8; //270;
  pnlDisplay.Height := Height - pnlDisplay.Top - bbtnBack.Height - 16;
  bbtnBack.Top := Height - bbtnBack.Height - 8;
  bbtnDelete.Top := Height - bbtnDelete.Height - 8;
  bbtnRestore.Top := Height - bbtnRestore.Height - 8;
  bbtnAdd.Top := Height - bbtnAdd.Height - 8;
  bbtnClose.Top := Height - bbtnClose.Height - 8;
  bbtnClose.Left := Width - bbtnClose.Width - 8;
end;

procedure TfrmSelectPlan.bbtnDeleteClick(Sender: TObject);
begin
  lModifications := True;
  if ((lstStatus[lbItems.ItemIndex]) = CST_ADDED) and not FileExists(ActualValue(lbItems)) then
  begin
    lstStatus.Delete(lbItems.ItemIndex);
    lbItems.Items.Delete(lbItems.ItemIndex);
  end
  else
    lstStatus[lbItems.ItemIndex] := CST_DELETED;
  SetButtonsStatus();
  lbItems.Refresh;
end;

procedure TfrmSelectPlan.bbtnRestoreClick(Sender: TObject);
begin
  lModifications := True;
  lstStatus[lbItems.ItemIndex] := '';
  SetButtonsStatus();
  lbItems.Refresh;
end;

procedure TfrmSelectPlan.lbItemsDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  with (Control as TListBox) do
  begin
    Canvas.FillRect(Rect);
    if (Level = 1) and not (lstStatus.Count = 0) and (Index <= lstStatus.Count-1) then
    begin
      if (lstStatus[Index] = CST_ADDED) then
      begin
        Canvas.Font.Color := clGreen;
        Canvas.Font.Style := [fsBold];
      end
      else if (lstStatus[Index] = CST_DELETED) then
      begin
        Canvas.Font.Color := clRed;
        Canvas.Font.Style := [fsItalic, fsStrikeOut];
      end
    end
    else
    begin
      Canvas.Font.Color := clBlack;
      Canvas.Font.Style := [];
    end;
    Canvas.TextOut(Rect.Left,Rect.Top,Items[Index]);
  end;
end;

function TfrmSelectPlan.IsFileDeleted(_FileName : String) : Boolean;
begin
  Result := RightStr(ExtractOnlyFileName(_FileName), Length(CST_DELETED_EXTENSION)) = CST_DELETED_EXTENSION;
end;

function TfrmSelectPlan.IsFileAdded(_FileName : String) : Boolean;
begin
  Result := RightStr(ExtractOnlyFileName(_FileName), Length(CST_ADDED_EXTENSION)) = CST_ADDED_EXTENSION;
end;

procedure TfrmSelectPlan.ApplyModifications();
var
  nCount : Integer;
begin
  for nCount := 0 to lstStatus.Count-1 do
  begin
    if (lstStatus[nCount] = CST_DELETED) then
      if IsFileAdded(lbItems.Items[nCount]) then
        DeleteFile(lbItems.Items[nCount])
      else if not IsFileDeleted(lbItems.Items[nCount]) then
        RenameFile(
          lbItems.Items[nCount],
          ExtractFilePath(lbItems.Items[nCount]) +
            ExtractOnlyFileName(lbItems.Items[nCount]) + CST_DELETED_EXTENSION + '.' +
            ExtractOnlyFileExt(lbItems.Items[nCount]));
    if (lstStatus[nCount] = CST_ADDED) and not IsFileAdded(lbItems.Items[nCount]) then
      CopyFile(
        PAnsiChar(lbItems.Items[nCount]),
        PAnsiChar(FCurrentFolder +
          ExtractOnlyFileName(lbItems.Items[nCount]) + CST_ADDED_EXTENSION + '.' +
          ExtractOnlyFileExt(lbItems.Items[nCount])),
        True);
    if (lstStatus[nCount] = '') and IsFileDeleted(lbItems.items[nCount]) then
      RenameFile(
        lbItems.Items[nCount],
        ExtractFilePath(lbItems.Items[nCount]) +
          LeftStr(ExtractOnlyFileName(lbItems.Items[nCount]), Length(ExtractOnlyFileName(lbItems.Items[nCount])) - Length(CST_DELETED_EXTENSION)) + '.' +
          ExtractOnlyFileExt(lbItems.Items[nCount]));
  end;
end;

function TfrmSelectPlan.CheckModifications() : Boolean;
var
  nRet : Integer;
begin
  Result := True;
  if lModifications then
  begin
    nRet := MessageDlg('Des modifications ont �t� demand�es, voulez-vous les appliquer ?', mtConfirmation, [mbYes, mbNo, mbCancel], 0);
    if nRet = mrCancel then Result := False
    else if nRet = mrYes then ApplyModifications();
    lModifications := (nRet = mrNo);
  end;
end;

procedure TfrmSelectPlan.AddFile(_FileName : String);
begin
  lstStatus.Add(CST_ADDED);
  lbItems.ItemIndex := lbItems.Items.Add(PAnsiChar(_FileName));
  lModifications := True;
  SetButtonsStatus();
end;

procedure TfrmSelectPlan.bbtnAddClick(Sender: TObject);
var
  iniFile : TiniFile;
  sCurrentDir : String;
begin

  sCurrentDir := GetCurrentDir;
  if odAddFile.Execute then
  begin
    AddFile(odAddFile.FileName);
    iniFile := TIniFile.Create(AddSlash(ExtractFilePath(Application.ExeName)) + ExtractOnlyFileName(Application.ExeName) + CST_INI_EXTENSION);
    iniFile.WriteString(CST_CONFIG_SECTION, CST_LAST_IMAGES_FOLDER, ExtractFilePath(odAddFile.FileName));
    iniFile.Free();
  end;
  SetCurrentDir(sCurrentDir);

end;

end.
