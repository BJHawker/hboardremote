object frmSelectPlan: TfrmSelectPlan
  Left = 373
  Top = 261
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'T'#233'l'#233'commande'
  ClientHeight = 608
  ClientWidth = 791
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -27
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  DesignSize = (
    791
    608)
  PixelsPerInch = 96
  TextHeight = 32
  object bbtnClose: TBitBtn
    Left = 632
    Top = 553
    Width = 145
    Height = 41
    Anchors = [akRight, akBottom]
    Caption = '&Fermer'
    TabOrder = 0
    OnClick = bbtnCloseClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
      F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
      000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
      338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
      45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
      3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
      F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
      000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
      338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
      4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
      8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
      333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
      0000}
    NumGlyphs = 2
  end
  object lbItems: TListBox
    Left = 8
    Top = 8
    Width = 769
    Height = 249
    Style = lbOwnerDrawVariable
    ItemHeight = 32
    TabOrder = 1
    OnClick = lbItemsClick
    OnDrawItem = lbItemsDrawItem
  end
  object bbtnBack: TBitBtn
    Left = 8
    Top = 553
    Width = 145
    Height = 41
    Anchors = [akLeft, akBottom]
    Caption = '&Retour'
    Enabled = False
    TabOrder = 2
    OnClick = bbtnBackClick
    Kind = bkIgnore
  end
  object pnlDisplay: TPanel
    Left = 8
    Top = 264
    Width = 769
    Height = 281
    TabOrder = 3
    object imgMain: TImage
      Left = 1
      Top = 1
      Width = 768
      Height = 279
      Stretch = True
    end
  end
  object mpMain: TMediaPlayer
    Left = 176
    Top = 488
    Width = 253
    Height = 30
    AutoRewind = False
    Display = pnlDisplay
    Visible = False
    TabOrder = 4
    OnNotify = mpMainNotify
  end
  object bbtnDelete: TBitBtn
    Left = 336
    Top = 553
    Width = 169
    Height = 41
    Anchors = [akLeft, akBottom]
    Caption = '&Supprimer'
    TabOrder = 5
    Visible = False
    OnClick = bbtnDeleteClick
    Kind = bkCancel
  end
  object bbtnRestore: TBitBtn
    Left = 336
    Top = 553
    Width = 169
    Height = 41
    Anchors = [akLeft, akBottom]
    Caption = '&Restaurer'
    TabOrder = 6
    Visible = False
    OnClick = bbtnRestoreClick
    Kind = bkRetry
  end
  object bbtnAdd: TBitBtn
    Left = 160
    Top = 553
    Width = 169
    Height = 41
    Anchors = [akLeft, akBottom]
    Caption = '&Ajouter'
    TabOrder = 7
    Visible = False
    OnClick = bbtnAddClick
    Kind = bkYes
  end
  object odAddFile: TOpenDialog
    Filter = 
      'Fichiers Images|*.bmp; *.gif; *.jpg; *.png|Fichiers Videos|*.wmv' +
      '; *.mpg; *.mpeg|Tous Fichiers|*.*'
    Left = 256
    Top = 184
  end
end
