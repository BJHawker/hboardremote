unit uCSV2DBfunctions;

interface

uses Classes, SysUtils;

function LoadCSVinStrings(aFileName:string): string;
function GetCSVcell(aLine: string; aIndex: integer): string;

implementation

function LoadCSVinStrings(aFileName:string): string;
var
  ts:TStringList;
begin
  ts:=TStringList.Create();
  ts.LoadFromFile(aFileName);
  while ts[ts.Count-1]='' do
  begin
    ts.Delete(ts.Count-1);
  end;
  result:=ts.Text;
  ts.Free();
end;

function GetCSVcell(aLine: string; aIndex: integer): string;
var
  ts:TStringList;
begin
  ts:=TStringList.Create();
  ts.Text:=StringReplace(aLine,';',#13#10,[rfReplaceAll]);
  try
    result:=ts[aIndex];
  except
    result:='';
  end;
  ts.Free();
end;

end.
 