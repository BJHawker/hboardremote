program hBoardRemote;

uses
  Forms,
  wndSelectPlan in 'wndSelectPlan.pas' {frmSelectPlan},
  uHBoardFunctions in 'uHBoardFunctions.pas',
  uBoardLiteCommon in 'uBoardLiteCommon.pas',
  uCSV2DBfunctions in 'uCSV2DBfunctions.pas',
  GraphicEx in '..\..\..\..\..\..\Program Files (x86)\Borland\Delphi7\Lib\GraphicEx\GraphicEx.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmSelectPlan, frmSelectPlan);
  Application.Run;
end.
