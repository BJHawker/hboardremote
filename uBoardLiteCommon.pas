unit uBoardLiteCommon;

interface

uses Graphics, Windows, MultiMon;

type
  TMediaType = (mtVideo, mtPicture, mtFlash, mtUndefined);

function CheckDynamicCaption(aCaption: string; aMedia:string): string;
function HexToColor(aHex: string): TColor;
function ColorToHex(Inp: TColor): String;
function MediaType(_MediaFileName : String): TMediaType;
function GetArchiveFileName : String;
function cheatMonitorFromWindow(hWnd: HWND; dwFlags: DWORD): HMONITOR; stdcall;
function ForceForegroundWindow(WndHandle: HWND): Boolean;

implementation

uses SysUtils, Classes, uCSV2DBfunctions, Forms;

function MediaType(_MediaFileName : String): TMediaType;
var
  sExt : String;
begin
  sExt := LowerCase(ExtractFileExt(_MediaFileName));
  if (sExt='.jpg') or (sExt='.bmp') or (sExt='.png') then
    Result := mtPicture
  else if (sExt='.avi') or (sExt='.wmv') or (sExt='.mpg') then
    Result := mtVideo
  else if sExt='.swf' then
    Result := mtFlash
  else
    Result := mtUndefined;
end;

function CheckDynamicCaption(aCaption: string; aMedia:string): string;
var
  i:integer;
  strNum:string;
  strOpe:string;
  Products:TStringList;
begin
  result:=aCaption;
  // @
  if Copy(aCaption, 1, 1) = '@' then
  begin
    strOpe:=Copy(aCaption, 2, 2);
    strNum:=Copy(aCaption, 4, Length(aCaption) - 3);
    Products:=TStringList.Create();
    Products.Text:=LoadCSVinStrings(ExtractFilePath(Application.Exename)+'pos.csv');
    i:=0;
    while i<Products.Count do
    begin
      if GetCSVcell(Products[i],0)=strNum then
      begin
        if strOpe='SD' then
        begin
          result:=GetCSVcell(Products[i], 1);
        end
        else if strOpe='LD' then
        begin
          result:=GetCSVcell(Products[i], 2);
        end
        else if strOpe='P1' then
        begin
          result:=GetCSVcell(Products[i], 3);
        end
        else if strOpe='P2' then
        begin
          result:=GetCSVcell(Products[i], 4);
        end
        else if strOpe='P3' then
        begin
          result:=GetCSVcell(Products[i], 5);
        end
        else if strOpe='P4' then
        begin
          result:=GetCSVcell(Products[i], 6);
        end
        else if strOpe='P5' then
        begin
          result:=GetCSVcell(Products[i], 7);
        end;
        if GetCSVcell(Products[i], 8) = '0' then
        begin
          result:='/'+result; // Not Available
        end;
      end;
      i:=i+1;
    end;
    Products.Free();
  end;
end;

function RGBToColor(r, g, b: integer): TColor;
begin
  Result := r + 256 * g + 256 * 256 * b;
end;

function HexToColor(aHex: string): TColor;
var
  r, g, b: integer;
  function HexToInt(aChar: char): byte;
  begin
    case aChar of
      '0'..'9': Result := ord(aChar) - 48;
      'A'..'Z': Result := ord(aChar) - 65 + 10;
      'a'..'z': Result := ord(aChar) - (65 + 32) + 10;
    else Result := 0;
    end;
  end;
begin
  if aHex[1] = '#' then
  begin
    aHex := copy(aHex, 2, length(aHex) - 1);
  end;
  r := 16 * HexToInt(aHex[1]) + HexToInt(aHex[2]);
  g := 16 * HexToInt(aHex[3]) + HexToInt(aHex[4]);
  b := 16 * HexToInt(aHex[5]) + HexToInt(aHex[6]);
  Result := RGBToColor(r, g, b);
end;

function ColorToHex(Inp: TColor): String;
var
  tmp: Longint;
begin
  tmp:=ColorToRGB(inp);
  Result:=IntToHex(GetRValue(tmp),2) + IntToHex(GetGValue(tmp),2) + IntToHex(GetBValue(tmp),2);
end;

function GetArchiveFileName : String;
begin
  DateTimeToString(Result, 'yyyymmddhhnnsszzz', Now());
end;

function cheatMonitorFromWindow(hWnd: HWND; dwFlags: DWORD): HMONITOR; stdcall;
begin
  // Does nothing, returns zero to force invalidate
  Result:=0;
end;

function ForceForegroundWindow(WndHandle: HWND): Boolean;
var
  CurrThreadID: DWORD;
  ForeThreadID: DWORD;
begin
  Result := True;
  if (GetForegroundWindow <> WndHandle) then
  begin
    CurrThreadID := GetWindowThreadProcessId(WndHandle, nil);
    ForeThreadID := GetWindowThreadProcessId(GetForegroundWindow, nil);
    if (ForeThreadID <> CurrThreadID) then
    begin
      AttachThreadInput(ForeThreadID, CurrThreadID, True);
      Result := SetForegroundWindow(WndHandle);
      AttachThreadInput(ForeThreadID, CurrThreadID, False);
      if Result then
        Result := SetForegroundWindow(WndHandle);
    end
    else
      Result := SetForegroundWindow(WndHandle);
  end;
end;

end.
